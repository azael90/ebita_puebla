package com.latinofuneral.bitacorasPuebla.Utils;


@SuppressWarnings("FieldCanBeLocal")
public final class BuildConfig {

    private static Branches targetBranch = Branches.PUEBLA;

    @SuppressWarnings("SpellCheckingInspection")
    public static enum Branches{PUEBLA}

    private BuildConfig(){}

    public static Branches getTargetBranch(){
        return targetBranch;
    }

}
