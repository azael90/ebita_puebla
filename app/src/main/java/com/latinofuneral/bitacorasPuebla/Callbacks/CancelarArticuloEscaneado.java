package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface CancelarArticuloEscaneado {
    public void onClickCancelarArticulo(int position, String id);
}
