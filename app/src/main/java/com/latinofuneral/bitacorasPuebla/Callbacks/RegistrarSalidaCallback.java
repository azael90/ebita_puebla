package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface RegistrarSalidaCallback {
    public void onClickRegistrarSalida(int position, String bitacora, String destino);
}
