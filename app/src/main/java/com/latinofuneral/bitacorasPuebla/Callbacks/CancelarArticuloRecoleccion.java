package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface CancelarArticuloRecoleccion {
    public void onClickCancelarArticuloRecoleccion(int position, String serie, String fecha, String bitacora);
}
