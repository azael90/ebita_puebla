package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface RequerirEventoDeSalidaCallback {
    public void onRequeriedEventoSalida(int position, String bitacora);
}
