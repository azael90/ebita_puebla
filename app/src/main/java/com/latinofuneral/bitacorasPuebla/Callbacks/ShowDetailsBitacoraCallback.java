package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface ShowDetailsBitacoraCallback {
    public void onClickShowDetails(int position, String bitacora);
}
