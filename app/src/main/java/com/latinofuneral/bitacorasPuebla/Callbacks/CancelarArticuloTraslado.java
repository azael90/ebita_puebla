package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface CancelarArticuloTraslado {
    public void onClickCancelarArticuloTraslado(int position, String serie, String fecha, String bitacora);
}
