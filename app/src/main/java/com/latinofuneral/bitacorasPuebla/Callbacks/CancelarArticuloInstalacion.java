package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface CancelarArticuloInstalacion {
    public void onClickCancelarArticuloInstalacion(int position, String serie, String fecha, String bitacora);
}
