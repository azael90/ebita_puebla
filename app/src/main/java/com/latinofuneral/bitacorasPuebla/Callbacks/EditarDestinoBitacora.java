package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface EditarDestinoBitacora {
    public void onClickEditarDestinoBitacora(int position, String bitacora);
}
