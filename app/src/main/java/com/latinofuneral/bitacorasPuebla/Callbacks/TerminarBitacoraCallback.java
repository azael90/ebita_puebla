package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface TerminarBitacoraCallback {
    public void onClickTerminarBitacora(int position, String bitacora);
}
