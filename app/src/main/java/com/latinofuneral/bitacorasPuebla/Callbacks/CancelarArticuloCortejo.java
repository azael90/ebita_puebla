package com.latinofuneral.bitacorasPuebla.Callbacks;

public interface CancelarArticuloCortejo {
    public void onClickCancelarArticuloCortejo(int position, String serie, String fecha, String bitacora);
}
